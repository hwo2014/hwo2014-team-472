{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}

module Main where

import           System.Environment         (getArgs)
import           System.Exit                (exitFailure)

import           Control.Applicative
import           Control.Monad
import           Data.Aeson                 (FromJSON (..), Result (..),
                                             Value (..), decode, eitherDecode,
                                             fromJSON, parseJSON, (.:))
import qualified Data.ByteString.Lazy.Char8 as L
import           Data.List
import           Network                    (PortID (..), connectTo)
import           System.IO                  (BufferMode (..), Handle, hGetLine,
                                             hPutStrLn, hSetBuffering)

import           Control.Concurrent.MVar
import qualified Data.Map                   as M
import           Data.Maybe                 (fromJust, isJust)

import qualified CarPositionsModel          as CP
import           GameInitModel
import           Control.Monad (when)

type ClientMessage = String

data Side = GoRight | GoLeft

instance Show Side where
  show GoRight = "Right"
  show GoLeft = "Left"

joinMessage botname botkey = "{\"msgType\":\"join\",\"data\":{\"name\":\"" ++ botname ++ "\",\"key\":\"" ++ botkey ++ "\"}}"
throttleMessage amount = "{\"msgType\":\"throttle\",\"data\":" ++ (show amount) ++ "}"
doSwitch side = "{\"msgType\": \"switchLane\", \"data\": \"" ++ show side ++ "\"}"
pingMessage = "{\"msgType\":\"ping\",\"data\":{}}"

connectToServer server port = connectTo server (PortNumber (fromIntegral (read port :: Integer)))

main = do
  args <- getArgs
  case args of
    [server, port, botname, botkey] -> do
      run server port botname botkey
    _ -> do
      putStrLn "Usage: hwo2014bot <host> <port> <botname> <botkey> "
      exitFailure

run server port botname botkey = do
  h <- connectToServer server port
  hSetBuffering h LineBuffering
  hPutStrLn h $ joinMessage botname botkey
  handleMessages h newGameState

handleMessages h gs = do
  msg <- hGetLine h
  case decode (L.pack msg) of
    Just json ->
      let decoded = fromJSON json >>= decodeMessage in
      case decoded of
        Success serverMessage -> handleServerMessage h serverMessage gs
        Error s -> fail $ "Error decoding message: " ++ s
    Nothing -> do
      fail $ "Error parsing JSON: " ++ (show msg)

data ServerMessage = Join | GameInit GameInitData | CarPositions [CP.CarPosition] | YourCar CarId | Unknown String Value
    deriving (Show)

data GameState = GameState {
    mycarid     :: Maybe CarId,
    mypieces    :: [Piece],
    mypositions :: [Float],
    myspeeds :: [Float],
    lastStraightSpeed :: Float,
    slowDowns :: [[Float]],
    laneOffset :: Int -> Float
    --mylanes :: M.Map Int [Lane]
}


knownSlowDowns = [[9.282959, 9.297607, 9.311523, 9.325439, 9.138672, 8.956055, 8.7771, 8.601318, 8.429199, 8.260986, 8.095459, 7.9335938, 7.7749023, 7.619629, 7.467041, 7.317627, 7.1713867, 7.028076]]

newGameState = GameState Nothing [] [] [] 0.0 knownSlowDowns (\_ -> 0)

handleServerMessage :: Handle -> ServerMessage -> GameState -> IO ()
handleServerMessage h serverMessage gs = do
  (responses, gs') <- respond serverMessage gs
  forM_ responses $ hPutStrLn h
  handleMessages h gs'

formatLog :: CP.CarPosition -> String
formatLog carPosition = color (CP.carId carPosition) ++ " - " ++ (show $ CP.pieceIndex pp) ++ " - " ++ (show $ CP.inPieceDistance pp)
    where pp = CP.piecePosition carPosition

distanceParcours = sum . map longueurPiece

longueurPiece p = case pieceLength p of
    Just l -> l
    Nothing -> 2 * 3.14159 * r * (a / 360.0)
  where r = fromIntegral $ fromJust $ radius p
        a = abs $ fromJust $ pieceAngle p

absolutePosition :: [Piece] -> Int -> Int -> Float -> Float
absolutePosition pieces l n o = (fromIntegral l) * (distanceParcours pieces) + (distanceParcours $ take n pieces) + o

isStraight = isJust . pieceLength

isAlmostStraight p = isStraight p || (abs $ fromJust $ pieceAngle p) <= 30

isCurve = not . isAlmostStraight

straightLineRemaining index piecesL offset
    | isStraight presentPiece = (distanceParcours $ takeWhile isAlmostStraight (drop index (cycle piecesL))) - offset
    | otherwise = 0
 where presentPiece = piecesL !! index

maxSpeed :: Piece -> Float -> Float
maxSpeed p offset
    | isStraight p = 10.0
    | (fromIntegral $ fromJust $ radius p) == 200.0 = 10.0
    | (fromIntegral $ fromJust $ radius p) + offset >= 110.0 = 6.7
    | (fromIntegral $ fromJust $ radius p) + offset >= 90.0 = 6.5
    | otherwise = 6.0



tactic :: Float -> Piece -> Float -> Float -> Float -> Float -> Float
tactic lineR piece speed lastStraightSpeed radialOffset angle
    | (abs angle) > 50 = 0.5
    | lineR == 0 = (maxSpeed piece radialOffset) / 10.0
    | lineR < 150 && speed > 7 = 0.0
    | lineR < 150 && speed <= 7 = 0.5
    | lineR >= 150 = 1.0
  where offset = 10 -- TODO

info :: Float -> Float -> Float -> Float -> Float -> Piece -> Float -> Float -> IO ()
info position speed lss lineR throttle piece lane angle = putStrLn $ (show position) ++ " - " ++ (show speed) ++ " - " ++ (show lss) ++ " - " ++ (show lineR) ++ " - " ++ (show throttle) ++ " - " ++ (show piece) ++ " - " ++ (show lane) ++ " - " ++ (show angle)



respond :: ServerMessage -> GameState -> IO ([ClientMessage], GameState)
respond message gs = case message of
  Join -> do
    putStrLn "Joined"
    return ([pingMessage] , gs)
  GameInit gameInit -> do
    putStrLn $ "GameInit: " ++ (reportGameInit gameInit)
    putStrLn $ "GameInit: " ++ (show gameInit)
    putStrLn $ show (piecesOfGame gameInit)
    let pieces = piecesOfGame gameInit
    --print $ map longueurPiece (piecesOfGame gameInit)
    --print $ distanceParcours (piecesOfGame gameInit)
    --modifyMVar_ (mypositions gs) (\_ -> return [0.0,0.0 :: Float])
    let laneOffsets = M.fromList $ map (\l -> (index l, distanceFromCenter l)) (lanesOfGame gameInit)
    let laneOffset = \n -> (fromIntegral $ (M.!) laneOffsets n)
    return ([pingMessage], gs {mypieces = pieces, laneOffset = laneOffset})
  YourCar c -> do
    return ([pingMessage] , gs {mycarid = Just c})
  CarPositions carPositions -> do
    let Just mycar = mycarid gs
    let mycarposition = head (filter (\cp -> CP.carId cp == mycar) carPositions)
    let CP.PiecePosition pieceIndex inPieceDistance lane lap = CP.piecePosition mycarposition
    let myangle = CP.angle mycarposition

    let piecesL = mypieces gs
    let piece = piecesL !! pieceIndex
    let position = absolutePosition piecesL lap pieceIndex inPieceDistance
    let speed = if length (mypositions gs) > 0 then position-head (mypositions gs) else 0
    let lastStraightSpeed' = if isStraight piece then speed else lastStraightSpeed gs

    let lineR = straightLineRemaining pieceIndex piecesL inPieceDistance
    let radialOffset = case pieceAngle piece of
	 Nothing -> 0.0
	 Just a -> ((laneOffset gs) (CP.startLaneIndex lane)) * (sign a) * (-(1.0))
    let throttle = tactic lineR piece speed lastStraightSpeed' radialOffset myangle
    info position speed lastStraightSpeed' lineR throttle piece radialOffset myangle

    let gs' = gs {mypositions = position:(mypositions gs), myspeeds = speed:(myspeeds gs), lastStraightSpeed=lastStraightSpeed'}
    return $ ([throttleMessage throttle, doSwitch GoRight], gs')

  Unknown msgType msgData -> do
    putStrLn $ "Unknown message: " ++ msgType
    print message
    return ([pingMessage], gs)

-- gameStart, lapFinished, finish, gameEnd, tournamentEnd
safeHead [] = Nothing
safeHead (x:xs) = Just x

sign :: Float -> Float
sign a = if a >= 0 then 1.0 else (-1.0)

decodeMessage :: (String, Value) -> Result ServerMessage
decodeMessage (msgType, msgData)
  | msgType == "join" = Success Join
  | msgType == "gameInit" = GameInit <$> (fromJSON msgData)
  | msgType == "carPositions" = CarPositions <$> (fromJSON msgData)
  | msgType == "yourCar" = YourCar <$> (fromJSON msgData)
  | otherwise = Success $ Unknown msgType msgData

instance FromJSON a => FromJSON (String, a) where
  parseJSON (Object v) = do
    msgType <- v .: "msgType"
    msgData <- v .: "data"
    return (msgType, msgData)
  parseJSON x          = fail $ "Not an JSON object: " ++ (show x)

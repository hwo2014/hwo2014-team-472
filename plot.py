from pylab import *


def diff(a, b):
    return [(v6-v) for (v6, v) in zip(a, b)]
#absolutes = [float(l.split(" ")[1]) for l in lines if l.startswith("Absolute ")]

def p(s):
    s = [x for x in s if x > 0]
    t = arange(0.0, len(s)/60., 1/60.)
    plot(t, s)

lines = filter(None, open("log6.txt").read().split("\n"))
vitesses6 = [float(l.split(" ")[1]) for l in lines if l.startswith("Vitesse ")]

lines = filter(None, open("log05.txt").read().split("\n"))
vitesses5 = [float(l.split(" ")[1]) for l in lines if l.startswith("Vitesse ")]

#lines = filter(None, open("log01.txt").read().split("\n"))
#vitesses01 = [float(l.split(" ")[1]) for l in lines if l.startswith("Vitesse ")]

lines = filter(None, open("log09.txt").read().split("\n"))
vitesses9 = [float(l.split(" ")[1]) for l in lines if l.startswith("Vitesse ")]

reponse_expo09 = [9*(1-e**(-x/0.8)) for x in arange(0.0, 100.0, 1/60.)]
reponse_expo06 = [6*(1-e**(-x/0.8)) for x in arange(0.0, 100.0, 1/60.)]
reponse_expo05 = [5*(1-e**(-x/0.8)) for x in arange(0.0, 100.0, 1/60.)]
reponse_expo01 = [1*(1-e**(-x/0.8)) for x in arange(0.0, 100.0, 1/60.)]

n = 700
#p(vitesses01[:n])
p(vitesses9[:n])
p(vitesses5[:n])
p(vitesses6[:n])
#p(diff(vitesses6, vitesses01)[:n])
p(reponse_expo05[:n])
p(reponse_expo06[:n])
#p(reponse_expo01[:n])
p(reponse_expo09[:n])

xlabel('time (s)')
ylabel('voltage (mV)')
title('About as simple as it gets, folks')
grid(True)
savefig("test.png")
show()


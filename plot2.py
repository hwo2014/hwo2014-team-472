from pylab import *

t = arange(0.0, 11.0, 1)
#s = sin(2*pi*t)
s = [0.0, 0.15727599999999953, 0.3114070000000009, 0.4624579999999998, 0.610481, 0.7555499999999995, 0.8977165999999999, 1.0370369999999998, 1.1735764, 1.3073730000000001, 1.4385070000000004]
#s2 = range(0.0, 100, 0.157276)[:len(s)]
s3 = [8.863823*(1-e**(-t/55.9)) for t in range(11)]

d = [b-a for (b,a) in zip(s3,s)]
print d
print sum(d)

#print len(t)
#print len(s)
#print len(s2)
#print len(s3)

#print s
#print s2
#print s3

if 0:
    plot(t, s)
    #plot(t, s2)
    plot(t, s3)
    
    xlabel('time (s)')
    ylabel('voltage (mV)')
    title('About as simple as it gets, folks')
    grid(True)
    savefig("test.png")
    show()
